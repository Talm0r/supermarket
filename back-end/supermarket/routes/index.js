var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var session = require('express-session')
var app = express();
const queryDb = (req, res, next, query) => {
  con.query(query, function (err, result, fields) {
    if (err) throw err;

    res.json(result);
  });
};
// Connect to database
var con = mysql.createConnection({

  host: "localhost",
  user: "root",
  password: "",
  database: "supermarket",
  timezone: 'utc' 
});


// Get all Products

router.get('/products', function (req, res, next) {
  queryDb(req, res, next, "SELECT categories.categoryname,products.id, products.productname, products.price, products.image FROM categories RIGHT JOIN products ON categories.categoryid = products.categoryid")


})

// Get all Categories

router.get('/categories', function (req, res, next) {
  queryDb(req, res, next, "SELECT * FROM categories")



})
// Search specific items 

router.get('/search', function (req, res, next) {
  queryDb(req, res, next, "SELECT * FROM `products` WHERE productname LIKE '%" + req.query.search + "%'")

})

// Get specific category 
router.get('/getcategory', function (req, res, next) {
  queryDb(req, res, next, "SELECT * FROM `products` WHERE categoryid = " + req.query.categoryid)

})
// Get Specific Item 
router.get('/getproduct', function (req, res, next) {
  queryDb(req, res, next, "SELECT * FROM `products` WHERE id = " + req.query.id)

})

// Get User Cart 
router.get('/usercart', function (req, res, next) {
  queryDb(req, res, next, "SELECT t2.cartid,t1.date,t2.productid,t3.image,t2.ammount,t2.ammount*t3.price as price,t3.productname,t4.name from cart t1 inner join cartitem t2 on t1.id =t2.cartid inner join products t3 on t3.id = t2.productid inner join user t4 on t1.customerid = t4.id where t4.id = " + req.query.id +" and t2.cartid = (select max(id) from cart where customerid= t4.id)")

})
// Check last user order 
router.get('/checkorder', function (req, res, next) {

  queryDb(req,res,next, "SELECT * FROM `order` WHERE customerid = "+req.query.customerid+" ORDER BY ID DESC LIMIT 1 ")


})

// Get user details
router.get('/getuserdetails', function (req, res, next) {
  queryDb(req, res, next,"SELECT * FROM user where id = " + req.query.id)

})

// Get all orders 
router.get('/getorders', function (req, res, next) {
  queryDb(req, res, next, "SELECT * from `order`")


})
// Add Product To Database
router.post('/addproduct', function (req, res, next) {
  queryDb(req, res, next, "INSERT INTO products (productname, categoryid, price, image) VALUES ('" + req.body.productname + "',' " + req.body.categoryid + "',' " + req.body.price + "','" + req.body.image + "')")



})

// Add item to cart 
router.post('/addtocart',function (req,res,next) {
  queryDb(req,res,next, "INSERT INTO `cartitem` ( `productid`, `ammount`, `price`, `cartid`) VALUES ("+ req.body.productid+","+req.body.ammount+","+req.body.price+","+req.body.cartid+");")
})





  // Update Product 

router.post('/updateprod', function (req, res, next) {
  
  queryDb(req, res, next, "  UPDATE products SET productname = '" + req.body.productname + "',categoryid = " + req.body.categoryid + ",price=" + req.body.price + ",image='" + req.body.image + "' WHERE id = " + req.body.id)
})

// Add Order To Database
router.post('/setorder', function (req, res, next) {
  queryDb(req, res, next, "INSERT INTO `order`  (customerid, cartid, price, deliverycity,deliverystreet,deliverydate,orderdate,creditcard) VALUES (" + req.body.customerid + ", " + req.body.cartid + ", " + req.body.price + ",'" + req.body.deliverycity + "','" + req.body.deliverystreet + "','" + req.body.deliverydate + "','" + req.body.orderdate + "'," + req.body.creditcard + ")")

})


// Register New User
router.post('/register', function (req, res, next) {
  queryDb(req, res, next, "INSERT INTO user  (id,name, lastname, email, password,city,street,role) VALUES ('" + req.body.id + "','" + req.body.name + "','" + req.body.lastname + "','" + req.body.email + "','" + req.body.password + "','" + req.body.city + "','" + req.body.street + "','" + req.body.role + "')")



})

// Check if id exists

router.get('/checkid',function(req,res,next) {
  console.log("SELECT * from `user` WHERE id =" +req.query.id)
  queryDb(req,res,next,"SELECT * from `user` WHERE id = " +req.query.id)
})

// Create New Category
router.post('/newcategory', function (req, res, next) {
  queryDb(req, res, next, "INSERT INTO categories  (categoryname, categoryid) VALUES (" + req.body.categoryname + "," + req.body.categoryid + ")")

})
// Create new cart 

router.get('/createcart', function(req,res,next){
  con.query("INSERT INTO cart (customerid,date) VALUES ("+req.query.customerid+",'"+req.query.date+"')" 
  ,
  function (err,result,fields) {
    if(err) throw err;
    else {
      con.query("SELECT LAST_INSERT_ID(id) as cartid FROM cart ORDER by id DESC limit 1"
      ,
      function(err,result,fields) {
        if(err) throw err;
        else {
          res.json(result);
        }
      })

    }
  })
 
 })
// Make user Receipt 

router.get('/receipt',function(req,res,next) {
  queryDb(req,res,next,"SELECT t2.cartid,t1.date,t2.productid,t3.image,t2.ammount,t2.ammount*t3.price as price,t3.productname,t4.name from cart t1 inner join cartitem t2 on t1.id =t2.cartid inner join products t3 on t3.id = t2.productid inner join user t4 on t1.customerid = t4.id where t4.id = "+req.query.id+" and t2.cartid = (select max(id) from cart where customerid= t4.id) ")
})
 //Check user Login
router.post('/login', function (req, res, next) {
  con.query(
    "SELECT * FROM user WHERE email = '" + req.body.loginEmail + "'AND password = '" + req.body.loginPassword + "'"


    , function (err, result, fields) {
      if (err) throw err;
      else {
        res.json(result);       
      }
    });
})

// Check user email
router.get('/checkemail', function (req, res, next) {
  console.log(  "SELECT * FROM `user` WHERE email = '" + req.query.email+"'")
  con.query(
    "SELECT * FROM `user` WHERE email = '" + req.query.email+"'"


    , function (err, result, fields) {
      if (err) throw err;
      else {
        res.json(result);       
      }
    });
})



/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'dd' });
});











module.exports = router;
