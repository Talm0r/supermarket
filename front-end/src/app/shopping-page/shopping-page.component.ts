import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import { MessangerService } from '../shared/messanger.service';
import { Subscription } from 'rxjs';
import * as $ from 'jquery';
@Component({
  selector: 'app-shopping-page',
  templateUrl: './shopping-page.component.html',
  styleUrls: ['./shopping-page.component.css']
})
export class ShoppingPageComponent implements OnInit {
  loggedIn:boolean;
  maximize:boolean;
  listener:Subscription;
  constructor(private userService:UserService, private messanger:MessangerService) {
    this.loggedIn = this.userService.isLoggedIn();
    this.listener = this.messanger.getObservable().subscribe(data => {
      if (data.type == "minimize") { this.maximize = true; }})
   }
 
  ngOnInit() {
  }

  maximizeCart() {
    this.maximize = false;
  
    $(".appProd").removeClass("col-sm-12")
    $(".appProd").addClass("col-sm-8")
    $(".appCart").css("display","block");
    this.messanger.emit({type:"maximize"})
  }
 
  
  
loginPage() {
  location.href = "/"
}
}
