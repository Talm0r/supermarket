
import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductsService } from '../../../../shared/products.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessangerService } from '../../../../shared/messanger.service';
import { CartService } from '../../../../shared/cart.service';
import { DatePipe } from '@angular/common';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css'],

})
export class ItemsListComponent implements OnInit {
  @Input() product;
  admin: boolean;

  constructor(private messanger: MessangerService, private productsService: ProductsService, public dialog: MatDialog) { }

  ngOnInit() {
    if (sessionStorage.getItem('role') == "owner") {
      this.admin = true;
    }
    else {
      this.admin = false;
    }
  }
  // Open Modal & send product information 
  openDialog(): void {
    const dialogRef = this.dialog.open(modalComponent, {
      width: '350px',
      height: '350px',
      data: { name: this.product.productname, price: this.product.price, image: this.product.image, productid: this.product.id }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  // Check if admin or client, if admin will send to edit if client will open modal
  buyOrEditProduct(id) {
    if (this.admin == true) {
      this.messanger.emit({ type: "editProduct", payload: id });
    }
    else {
      this.productsService.getSpecificProduct(id).subscribe((data) => {
        this.openDialog();
      })
    }
  }
}

// Modal component
@Component({
  selector: 'dialog-overview-example-dialog',
  template: `<h1  mat-dialog-title> {{data.name}} </h1>
  <img style="max-width: 100px;" src="{{data.image}}">
  <div mat-dialog-content>
    <p>How many would you like to buy?</p>
    <form  [formGroup]="cartForm">
    <mat-form-field>
      <input matInput (change)="checkMin($event)" (keyup)="checkMin($event)" formControlName="ammount" type="number" [(ngModel)]="tempPrice">
     
     
      <small> Total price: {{tempPrice | priceCalc: data.price | currency: 'ILS'}}</small>
    </mat-form-field>
    </form>
  </div>
  <div mat-dialog-actions>
    <button mat-button class="btn btn-danger" (click)="onNoClick()">No Thanks</button>
    <button mat-button style="position:relative; left: 80px;" class="btn btn-success" *ngIf="isValid" (click)="addToCart()">Add to cart!</button>
  </div>
  `, providers: [DatePipe],
})
export class modalComponent {
  tempPrice: number;
  isValid:boolean;
  cartForm: FormGroup = new FormGroup({
    ammount: new FormControl("", [Validators.required]),
    productid: new FormControl(this.data.productid, [Validators.required]),
    price: new FormControl(),
    cartid: new FormControl("", Validators.required),
    image: new FormControl(this.data.image),
    productname: new FormControl(this.data.name)
  })
  constructor(
    private datePipe: DatePipe,
    private cartService: CartService,
    private messanger: MessangerService,
    public dialogRef: MatDialogRef<modalComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { console.log(data) }

  onNoClick(): void {
    this.dialogRef.close();
  }
  userCart = [];
  // Add item to cart
  addToCart(): void {
    if (localStorage.getItem("userCart") != null) {
      this.userCart = JSON.parse(localStorage.getItem("userCart"))
    } 
    let updated = false;
    localStorage.setItem("loggedIn",sessionStorage.loggedIn);
    for (let i = 0; i < this.userCart.length; i++) {
      if (this.cartForm.value.productid == this.userCart[i].productid) {
        this.userCart[i].ammount += this.cartForm.value.ammount
        updated = true;
      }
    }
      if(!updated) {
        
        this.cartForm.value.price = this.tempPrice * this.data.price
        this.cartForm.value.cartid = sessionStorage.loggedIn
 

        this.userCart.push(this.cartForm.value)
      }
   
    localStorage.setItem("userCart", JSON.stringify(this.userCart))
    
    if(localStorage.getItem("date") == null) {
      localStorage.setItem("date",JSON.stringify(new Date()))
    } 
    this.messanger.emit({ type: "itemAddedToCart" });
    this.dialogRef.close();
  }
  // Check minimum value & use enter to add
  checkMin(event) {
    if(this.cartForm.value.ammount > 0) {
      this.isValid = true;
    }
    else {
      this.isValid = false;
    }
    if(event.key == "Enter" && this.isValid == true) {
      this.addToCart()
    }
  }
}

