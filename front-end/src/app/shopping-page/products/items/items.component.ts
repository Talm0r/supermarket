import { Component, OnInit, Input, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ProductsService } from '../../../shared/products.service';
import { MessangerService } from '../../../shared/messanger.service';
import { Subscription } from 'rxjs';
import * as $ from 'jquery';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  products = [];
  listener: Subscription;
  admin: boolean;
  product;
  tempPrice: number;
  addToCartForm: FormGroup;
  touched: boolean;
  constructor(
    private productsService: ProductsService,
    public dialog: MatDialog,
     private messanger: MessangerService) {
    this.addToCartForm = new FormGroup({
      ammount: new FormControl("", [Validators.required])

    })
  }
  ngOnInit() {
// Get all products
    this.getProducts();
    if (sessionStorage.getItem('role') == "owner") {
      this.admin = true;
    }
    else {
      this.admin = false;
    }
// Listen to messanger for search \ category & update after added\updated item
    this.listener = this.messanger.getObservable().subscribe(data => {
      if(data.type == "searchProduct") {
       
          this.search(data.payload)
        }
      if(data.type == "categorySearch") {
        this.searchCategory(data.payload);
      }
      if(data.type == "addOrUpdate") {
        this.search("");
      }
      }
    )
  }
// Get all products from database
  getProducts() {
    this.productsService.getAllProducts().subscribe((data) => {
      this.products = data;
    });
  }
  // Search specific items
  search(term: string) {
    this.productsService.searchProducts(term).subscribe((data) => {
      this.products = data;
    });
  }
// Choose Category
  searchCategory(categoryId: number) {
    if(categoryId == 0) {
      this.productsService.searchProducts("").subscribe((data) => {
        this.products = data;
      });
    }
    else {
    this.productsService.searchCategory(categoryId).subscribe((data) => {
      this.products = data;
    });}
  }
}