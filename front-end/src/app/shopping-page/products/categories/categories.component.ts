import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../shared/products.service';
import { MessangerService } from '../../../shared/messanger.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories = [];
  constructor(private productsService:ProductsService ,private messanger:MessangerService) {  }

  ngOnInit() {
    this.getCategories()
  }

  getCategories() {

    this.productsService.getAllCategories().subscribe((data) => {
   
    this.categories = data;
    });
  }
  getCategory(id) {
    for(let i = 0; i<5; i++) {
      $("#"+i).removeClass("btn-danger")
    }
  $("#"+id).addClass("btn-danger")
    this.messanger.emit({type:"categorySearch",payload:parseInt(id)});
  }
}
