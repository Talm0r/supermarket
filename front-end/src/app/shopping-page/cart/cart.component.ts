import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductsService } from '../../shared/products.service';
import * as $ from 'jquery';
import { Subscription } from 'rxjs';
import { MessangerService } from '../../shared/messanger.service';
import { Product } from '../../models/product';
import { CartService } from '../../shared/cart.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  providers: [DatePipe]
})
export class CartComponent implements OnInit {
  admin: boolean;
  addProductForm: FormGroup;
  categories:any = [];
  userCart:any = [];
  addMode: boolean;
  editMode: boolean;
  updated: boolean;
  listener: Subscription;
  totalPrice: number = 0;
  totalItems: number = 0;
  minimize:boolean = true;
  added:boolean;
  

  editProduct;
  editProductForm: FormGroup;
  constructor(private datePipe: DatePipe,private cartService: CartService, private productsService: ProductsService, 
    private messanger: MessangerService) {
    this.addProductForm = new FormGroup({
      productname: new FormControl("", [Validators.required]),
      price: new FormControl("", [Validators.required]),
      image: new FormControl("", [Validators.required]),
      categoryid: new FormControl("1", [Validators.required])
    })

  }
// Check if user or owner logged in
  ngOnInit() {
    if (sessionStorage.getItem("role") != "owner") {
      this.getUserCart(sessionStorage.getItem("loggedIn"));
    }

    // If on admin mode, check if edit or add product 
    this.listener = this.messanger.getObservable().subscribe(data => {
      if (data.type == "editProduct") {
        this.productsService.getSpecificProduct(data.payload).subscribe((productToEdit) => {
          this.editProduct = productToEdit;
          this.editProductMode()
          this.editProductForm = new FormGroup({
            productname: new FormControl(productToEdit[0].productname, [Validators.required]),
            price: new FormControl(productToEdit[0].price, [Validators.required]),
            image: new FormControl("", [Validators.required]),
            categoryid: new FormControl(productToEdit[0].categoryid, [Validators.required]),
            id: new FormControl(productToEdit[0].id)
          })
        });
      }
      if (data.type == "updateCart") {
        this.getUserCart(sessionStorage.getItem("loggedIn"));
      }
      if(data.type == "itemAddedToCart") {
        this.getUserCart(sessionStorage.getItem("loggedIn"));
      }
      // Maximize\Minimize cart icon
      if(data.type == "maximize") {
        this.minimize = true;
      }

    })

    if (sessionStorage.getItem('role') == "owner") {
      this.admin = true;
    }
    else {
      this.admin = false;

    }
    $('#formId').change(
      function (e) {

        e.preventDefault();
      }
    );
    this.getCategories();
  }
  // Ref to order page
  makeOrder() {
    location.href = "/order"
  }
  // Minimize cart
  minimizeCart() {
    $(".appCart").css("display","none");
    $(".appProd").removeClass("col-sm-8")
    $(".appProd").addClass("col-sm-12")
    this.messanger.emit({ type: "minimize" });
    
    this.minimize = false;
  }

  // Get all categories 
  getCategories() {

    this.productsService.getAllCategories().subscribe((data) => {

      this.categories = data;
    });
  }

  // Get user  cart
  getUserCart(userId) {
    this.userCart = [];    
    this.totalItems = 0;
    this.totalPrice = 0;
    if(localStorage.getItem("userCart") != null) {
    let data = JSON.parse(localStorage.getItem("userCart"))
     this.userCart = data;
      for (let i = 0; i < data.length; i++) {
        this.totalPrice += data[i].price
        this.totalItems += data[i].ammount
      }
      localStorage.setItem("price",JSON.stringify(this.totalPrice))
    }
  }
  // Add new product form
  addNewProduct() {
    let temp = this.addProductForm.value.categoryid.split(" ")[0];
    this.addProductForm.value.categoryid = temp
    let x = this.addProductForm.value.image.split("fakepath")[1]
    x = 'assets/' + x
    this.addProductForm.value.image = x
    this.productsService.addProduct(this.addProductForm.value).subscribe((data) => {
      this.messanger.emit({ type: "addOrUpdate" });
      this.added = true;
      this.addMode = false;
    });
  }
  // Edit product
  editProductInDb() {
    if (this.editProductForm.value.image == "") {
      this.editProductForm.value.image = this.editProduct[0].image
    }
    else {
      let x = this.editProductForm.value.image.split("fakepath")[1]
      x = 'assets/' + x
      this.editProductForm.value.image = x
    }
    this.productsService.editProduct(this.editProductForm.value).subscribe((data) => {
      this.messanger.emit({ type: "addOrUpdate" });
      this.editMode = false;
      this.updated = true;
    })
  }
  addProductMode() {
    this.addMode = true;
    this.editMode = false;
    this.updated = false;
  }
  editProductMode() {
    this.addMode = false;
    this.editMode = true;
    this.updated = false;
  }

  // Delete item from cart
  deleteFromCart(productid) {
    let data = JSON.parse(localStorage.getItem("userCart"))
     for(let i = 0 ; i< data.length ; i++ ){
       if(data[i].productid == productid){
         this.removeA(this.userCart,this.userCart[i])
         localStorage.setItem("userCart", JSON.stringify(this.userCart))
       }
     }
  }

  // Delete all items from cart
  deleteAllItemsFromCart() {
    let x = confirm("Are you sure you want to delete all items from cart?")
    if(x) {
      localStorage.clear();
      this.messanger.emit({ type: "updateCart" }); 
  }
}

// Explode specific index from array
removeA(arr,b) {
  var what, a = arguments, L = a.length, ax;
  while (L > 1 && arr.length) {
      what = a[--L];
      while ((ax= arr.indexOf(what)) !== -1) {
          arr.splice(ax, 1);
      }
  }
  return arr;
}
}
