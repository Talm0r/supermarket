export class Product {
    id:number;
    name:string;
    categoryid:number;
    price:number;
    image:string;
}