export class User {
    id:number;
    name:string;
    lastname:string;
    email:string;
    password:string;
    city:string;
    street:string;
    role:string;
}