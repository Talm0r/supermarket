import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../shared/user.service';
import { CartService } from '../../shared/cart.service';
import { DatePipe } from '@angular/common';
import { MessangerService } from '../../shared/messanger.service';
import * as jsPDF from 'jspdf'

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  providers: [DatePipe]
})

export class DetailsComponent implements OnInit {
  
   
  maekOrderForm: FormGroup;
  loggedInUser:any;
  cartId: number;
  minDate = new Date();
  maxDate = new Date(2020, 0, 1);
  cities:any[] = ["Tel Aviv", "Netanya", "Rehovot", "Rishon Le-Ziyyon", "Ramat-Gan", "Eilat", "Akko", "Savyon"];
  fill = false;
  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Friday and Saturday from being selected.
    return day !== 5 && day !== 6;
  }
  constructor(private messanger: MessangerService, private datePipe: DatePipe, private userService: UserService, private cartService: CartService) {

    this.maekOrderForm = new FormGroup({
      customerid: new FormControl(sessionStorage.loggedIn),
      creditcard: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(13)
      ])),
      cartid: new FormControl(""),
      deliverycity: new FormControl("", [Validators.required]),
      deliverystreet: new FormControl("", [Validators.required]),
      deliverydate: new FormControl("", [Validators.required]),
      orderdate: new FormControl(new Date()),
      price: new FormControl("")

    }, Validators.required)

  }
// Get user details and save on loggedInUser
  ngOnInit() {
    this.getUserDetails();
  }

  getUserDetails() {
    this.userService.getDetails(sessionStorage.loggedIn).subscribe((data) => {

      this.loggedInUser = data;
    })
  }

// Make order
 
  makeOrder() {
    let userCart = JSON.parse(localStorage.getItem("userCart"));
    let cartDateCreated = this.datePipe.transform(JSON.parse(localStorage.getItem("date")), 'yy-MM-dd')
    this.cartService.createNewCart(sessionStorage.loggedIn, cartDateCreated).subscribe((data) => {
      this.cartId = data[0].cartid 
      for (let i = 0; i < userCart.length; i++) {   
       userCart[i].cartid = data[0].cartid
        this.cartService.addItemToCart(userCart[i]).subscribe((data) => {
          if(i+1 == userCart.length) {
          this.messanger.emit({ type: "orderMade" });
          localStorage.removeItem("userCart")
          }
        })
      }
        this.maekOrderForm.value.price = sessionStorage.totalprice;
        this.maekOrderForm.value.creditcard = this.maekOrderForm.value.creditcard.replace(/\s/g, '');
        this.maekOrderForm.value.cartid = data[0].cartid
        
        if (this.maekOrderForm.valid == true) {
          this.maekOrderForm.value.orderdate = this.datePipe.transform((this.maekOrderForm.value.orderdate), 'yy-MM-dd');
          this.maekOrderForm.value.deliverydate = this.datePipe.transform(this.maekOrderForm.value.deliverydate, 'yy-MM-dd');
          this.cartService.makeOrder(this.maekOrderForm.value).subscribe((data) => {
              this.messanger.emit({ type: "orderMade" });
              localStorage.removeItem("userCart")
            
          })
        }
    })
  }
}