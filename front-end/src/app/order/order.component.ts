import { Component, OnInit, Inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessangerService } from '../shared/messanger.service';
import { UserService } from '../shared/user.service';
import { CartService } from '../shared/cart.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import * as jsPDF from 'jspdf'

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  listener: Subscription;
  orderMade: boolean;
  loggedIn: boolean;
  receipt;
  constructor(private cartService: CartService, private userService: UserService, private messanger: MessangerService, public dialog: MatDialog) {
    this.loggedIn = this.userService.isLoggedIn();
  }

  ngOnInit() {

    this.listener = this.messanger.getObservable().subscribe(data => {
      if (data.type == "orderMade") {

        this.orderMade = true;
      }
    })
    if(sessionStorage.getItem("loggedIn") != null) {
    this.cartService.getReceipt(sessionStorage.loggedIn).subscribe((data) => {

      this.receipt = data;
    })
  }
  }

  loginPage() {
    location.href = "/"
  }

  viewReceipt() {
    if(sessionStorage.getItem("loggedIn") != null) {
    this.cartService.getReceipt(sessionStorage.loggedIn).subscribe((data)=>{
      let doc = new jsPDF;
      let b=10;
      let totalPrice = 0;
    for(let i =0; i<data.length; i++ ) {
    doc.text(data[i].ammount +" "+data[i].productname+ " Total - " +data[i].price +" Nis"  , 10, b)
    b+=10;
    totalPrice+= data[i].price
    
    }
    doc.text("Total "+ totalPrice + " Nis",10,b)
    doc.save('receipt.pdf')
    })
  }
  }


}
