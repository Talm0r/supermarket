import { Component, OnInit } from '@angular/core';
import { CartService } from '../../shared/cart.service';

@Component({
  selector: 'app-finalcart',
  templateUrl: './finalcart.component.html',
  styleUrls: ['./finalcart.component.css']
})
export class FinalcartComponent implements OnInit {
  userCart;
  totalPrice: number = 0;
  totalItems: number = 0;
  
  constructor(private cartService:CartService) { }
  // Get user cart and preview before buy
  ngOnInit() {
    if(sessionStorage.getItem("loggedIn") != null) {
    this.getUserCart(sessionStorage.loggedIn)
    }
  }
// Go back to shop
  goBack() {
    location.href = "/shop"
  }

  // Preview user cart + calculate total items and price
  getUserCart(userId) {
 
      let data = JSON.parse(localStorage.getItem("userCart"))
      this.userCart = data;
      this.totalItems = 0;
      this.totalPrice = 0;
      for (let i = 0; i < data.length; i++) {
        this.totalPrice += data[i].price
        this.totalItems += data[i].ammount     
      sessionStorage.setItem('totalprice',""+ this.totalPrice);
      }
  }
  search(term: string,cartId) {
    cartId = sessionStorage.loggedIn;
    this.cartService.searchCart(term,cartId).subscribe((data) => {
      this.userCart = data;
    });
  }
}
