import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalcartItemsComponent } from './finalcart-items.component';

describe('FinalcartItemsComponent', () => {
  let component: FinalcartItemsComponent;
  let fixture: ComponentFixture<FinalcartItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalcartItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalcartItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
