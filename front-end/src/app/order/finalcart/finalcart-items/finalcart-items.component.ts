import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-finalcart-items',
  templateUrl: './finalcart-items.component.html',
  styleUrls: ['./finalcart-items.component.css']
})
export class FinalcartItemsComponent implements OnInit {
  @Input() cartItem;
  constructor() { }

  ngOnInit() {
  }

}
