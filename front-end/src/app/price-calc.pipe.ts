import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priceCalc'
})
export class PriceCalcPipe implements PipeTransform {

  transform(value: number, value2?: number): number {
    return value*value2;
  }

}
