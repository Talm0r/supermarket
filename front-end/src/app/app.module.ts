import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginRegisterComponent } from './login-page/login-register/login-register.component';
import { AboutusComponent } from './login-page/aboutus/aboutus.component';
import { InfoComponent } from './login-page/info/info.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginPageComponent } from './login-page/login-page.component';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingPageComponent } from './shopping-page/shopping-page.component';
import { CartComponent } from './shopping-page/cart/cart.component';
import { ProductsComponent } from './shopping-page/products/products.component';
import { CategoriesComponent } from './shopping-page/products/categories/categories.component';
import { ItemsComponent } from './shopping-page/products/items/items.component';
import { OrderComponent } from './order/order.component';
import { FinalcartComponent } from './order/finalcart/finalcart.component';
import { DetailsComponent } from './order/details/details.component';
import { PriceCalcPipe } from './price-calc.pipe';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ItemsListComponent, modalComponent } from './shopping-page/products/items/items-list/items-list.component';
import {MatInputModule, MatToolbarModule, MatButtonModule, MatIconModule, MatListModule} from '@angular/material';
import {MDCList} from "@material/list";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSelectModule} from '@angular/material/select';
import { MatNativeDateModule} from '@angular/material/';
import { LayoutModule } from '@angular/cdk/layout';
import { HighlightDirective } from './highlight.directive';
import { FinalcartItemsComponent } from './order/finalcart/finalcart-items/finalcart-items.component';
import {CardModule} from 'ngx-card/ngx-card';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


const appRoutes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: 'shop' , component: ShoppingPageComponent},
  { path: 'order' , component: OrderComponent},
  
 
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }

];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginRegisterComponent,
    AboutusComponent,
    InfoComponent,
    LoginPageComponent,
    ShoppingPageComponent,
    CartComponent,
    ProductsComponent,
    CategoriesComponent,
    ItemsComponent,
    OrderComponent,
    FinalcartComponent,
    DetailsComponent,
    PriceCalcPipe,
    modalComponent,
    ItemsListComponent,
    HighlightDirective,
    FinalcartItemsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatInputModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatSelectModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    CardModule,
    
    MatAutocompleteModule,
    ReactiveFormsModule,RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ), LayoutModule, MatToolbarModule, MatButtonModule, MatIconModule, MatListModule

    
    
  ],
  entryComponents: [modalComponent],
  providers: [ MatNativeDateModule],
  bootstrap: [AppComponent]
})

export class AppModule { }
