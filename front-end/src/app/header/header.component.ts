import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms'; 
import {FormsModule} from '@angular/forms'
import * as $ from 'jquery';
import { MessangerService } from '../shared/messanger.service';
import { ProductsService } from '../shared/products.service';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  
})
export class HeaderComponent implements OnInit {
shoppingPage:boolean;
loggedInUser:string
loggedIn:boolean;
prod:any
  constructor(
    private messanger:MessangerService,
    private products: ProductsService,
    private userService:UserService
  ) { }

  ngOnInit() {

    // Check if user logged in, Prints the name on the navbar & Checks if on shopping page to enable search input
    this.loggedIn = this.userService.isLoggedIn()
    if(this.loggedIn == true) {
     this.loggedInUser = sessionStorage.getItem('loggedInName');
    }
    if(location.href.indexOf("shop") > 0 && this.loggedIn == true) {
      this.shoppingPage = true;
    }
    else {
      this.shoppingPage = false;
    }
  }
  // Emitting to products component search value, and removing selected category 
  searchProd() {
    for(let i = 1; i<5; i++) {
      $("#"+i).removeClass("btn-danger")
    }
    this.messanger.emit({type:"searchProduct",payload:this.prod});
  }

  // Log out - clear session and remove items from local storage
 logOut() {
  sessionStorage.clear();
  localStorage.removeItem("cartstatus")
  localStorage.removeItem("deliverydate")
  location.href = "/"
 }

 // Image directs to homepage
 homePage() {
   location.href = "/"
 }
}
