import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../shared/products.service';
import { UserService } from '../../shared/user.service';
import { MessangerService } from '../../shared/messanger.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
  loggedIn: boolean;
  totalProds: number;
  totalOrders: number;
  firstShop: boolean;
  listener: Subscription;
  resumeShopping: boolean;
  notFirstShop: boolean;
  orderdate: string;
  lastCartOpened: string;
  name: string;
  price: number;
  adminMode: boolean;
  constructor(private messanger: MessangerService, private productsService: ProductsService, private userService: UserService) { }

  ngOnInit() {

    // Getting all products and orders to preview 
    this.getProducts();
    this.getOrders();

    // Check if user is logged in
    if (sessionStorage.loggedIn == null) {
      this.loggedIn = false;
    }
    else {
      this.loggedIn = true;
    }
    this.name = sessionStorage.loggedInName
    // Check if owner logged in, if not checking in localstorage if theres an open cart, if there's none checking if its first buy or already ordered before
    if (sessionStorage.role == "owner") {
      this.adminMode = true;
    }
    else {
      if (localStorage.getItem("userCart") != null) {
        this.resumeShopping = true;
        this.lastCartOpened = localStorage.getItem("date").split("T")[0]
        this.price = JSON.parse(localStorage.getItem("price"))
      }
      else {
        switch (localStorage.getItem("cartstatus")) {
          case "opencart":
            this.resumeShopping = true;
            break;
          case "firstbuy":
            this.firstShop = true;
            break;
          case "closedcart":
            this.notFirstShop = true;
            this.orderdate = localStorage.getItem("orderdate").split("T")[0]
            break;
          default:
        }
      }
    }
  }
// Get all products and orders from database and counts them
  getProducts() {
    this.productsService.getAllProducts().subscribe((data) => {
      this.totalProds = data.length;     
    });
  }
  getOrders() {
    this.productsService.getAllOrders().subscribe((data) => {
      this.totalOrders = data.length;
    });
  }
}
