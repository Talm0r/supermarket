import { Component, OnInit, Inject } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/user';
import { UserService } from '../../shared/user.service';
import { CartService } from '../../shared/cart.service';
import { MessangerService } from '../../shared/messanger.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';


@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.css']
})




export class LoginRegisterComponent implements OnInit {
  loggedIn: boolean;
  signUpForm: FormGroup;
  signInForm: FormGroup;
  user: User;
  startShopping: boolean;
  resumeShopping: boolean;
  cartStatus: string;
  valid: boolean = false;
  wrongId: boolean
  wrongEmail: boolean
  wrongPassword: boolean
  invalidEmail: boolean;
  adminMode: boolean;
  validForm:boolean;
  constructor(private messangerService: MessangerService,
    private cartService: CartService,
    private userService: UserService,
    public dialog: MatDialog) {
    this.signInForm = new FormGroup({
      loginEmail: new FormControl("", [Validators.required]),
      loginPassword: new FormControl("", [Validators.required])
    })

    this.signUpForm = new FormGroup({
      id: new FormControl("", [Validators.required]),
      name: new FormControl("", [Validators.required]),
      lastname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
      city: new FormControl("", [Validators.required]),
      street: new FormControl("", [Validators.required]),
      role: new FormControl("client", [Validators.required])

    }, Validators.required)

  }

  // Register form - check if id exists
  checkUserId() {
    this.userService.checkIfIdExists(this.signUpForm.value.id).subscribe((data) => {
      if (data[0] != undefined) {
        this.wrongId = true;
      }
      else {
        this.wrongId = false;
      }
      this.checkValidFirstStep()
    }
    )
  }
// Register form - Check if both passwords are the same
  checkPass(confirmPass) {
    if ($("#confirmPassword").val() != this.signUpForm.value.password) {
      this.wrongPassword = true;
    }
    else {
      this.wrongPassword = false;
    }
    this.checkValidFirstStep()
  }
  checkValidFirstStep() {
    if (this.wrongPassword == false && this.wrongEmail == false && this.wrongId == false) {
      this.valid = true;
    }
    else {
      this.valid = false;
    }
  }
  // Register form - email validation
  validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }
  
// Register form - Check if user email is already registered
  checkUserEmail() {
    let x = this.validateEmail(this.signUpForm.value.email)
    if (!x) { this.invalidEmail = true; }
    if (x) {
      this.invalidEmail = false;
      this.userService.checkIfEmailExists(this.signUpForm.value.email).subscribe((data) => {

        if (data[0] != undefined) {

          this.wrongEmail = true;
        }
        else {
          this.wrongEmail = false;
        }
        this.checkValidFirstStep()
      })
    }
  }
// Register form - Changing modal to step 2 form
  nextStep() {
    $("#step1").addClass('hideMe');
    $("#step2").removeClass('hideMe');
    $("#registerModalLabel").text("Step 2 ")
  }
  ngOnInit() {
    this.loggedIn = this.userService.isLoggedIn()
    $(document).ready(function () {
      $(".btnEndStep1").click(function () {
        $("#step1").addClass('hideMe');
        $("#step2").removeClass('hideMe');
        $("#registerModalLabel").text("Step 2 ")

      })
    });
    // Check if logged in user is owner, if it is direct him to shop, else enable resume shopping\start shopping button
    if(sessionStorage.role == "owner") {
      this.adminMode = true; 
    }
    else {
    if (localStorage.getItem("userCart") != null) {
      this.resumeShopping = true;
    }
    else {
      switch (localStorage.getItem("cartstatus")) {
        case "opencart":
          this.resumeShopping = true;
          break;
        case "firstbuy":
          this.startShopping = true;
          break;
        case "closedcart":
          this.startShopping = true;
          break;
        default:
      }
    }
  }
  }

  // Register form - Send form details to database
  sendForm() {
    this.userService.addUser(this.signUpForm.value).subscribe((data) => {
      this.messangerService.emit({ type: "registered" });
      $("#step2").addClass('hideMe');
      $("#step3").removeClass('hideMe');
      $("#registerModalLabel").text("Register Complete")
    });
  }
    // Register form - Go back to step one 
    backToStep1() {
      $("#step2").addClass('hideMe');
      $("#step1").removeClass('hideMe');
      $("#registerModalLabel").text("Step 1")
    }
  

// Login form - Check user details , if success will set session and check if already has open cart
  checkLogin() {
    this.userService.checkLogin(this.signInForm.value).subscribe((data) => {

      if (data[0] == undefined) {

        alert("Wrong email or password")
      }
      else {
        var temp = data[0].name + " " + data[0].lastname
        sessionStorage.setItem('loggedIn', data[0].id);
        sessionStorage.setItem('loggedInName', temp);
        sessionStorage.setItem('role', data[0].role);
        if(localStorage.getItem("loggedIn") != null ) {
          if(localStorage.getItem("loggedIn") != data[0].id) {
            localStorage.clear();     
          }
        }
        if(data[0].role == "owner") {
          location.href = "/shop"
        }
        else {
        if (localStorage.getItem("userCart") != null) {
          // has an open cart              
          localStorage.setItem("cartstatus", "opencart")
        }
// Check if ordered before 
        else {
          this.cartService.checkLastOrder(data[0].id).subscribe((data) => {
            if (data.length == 0) {
              localStorage.setItem("cartstatus", "firstbuy")
            }
            else {
              localStorage.setItem("cartstatus", "closedcart")
              localStorage.setItem("orderdate", data[0].orderdate)
            }
          })
        }
        this.messangerService.emit({ type: "registered" });
        location.reload();
      }
    }

    })

  }
  // Go to shopping page
  shoppingPage() {
    location.href = "/shop"
  }
  



}

