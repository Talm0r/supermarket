import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class ProductsService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    contactMessagesEmitter: EventEmitter<string> = new EventEmitter();
    // contactEditEventEmiter: EventEmitter<User> = new EventEmitter();



    constructor(private http: HttpClient) {
    }
    // get products from database
    getAllProducts(): Observable<any> {
        return this.http.get("http://localhost:3000/products/");
    }
    // Get Specific Product 

    getSpecificProduct(productId): Observable<any> {
        return this.http.get("http://localhost:3000/getproduct?id="+productId);
    }
    // Search specific products 
    searchProducts(term): Observable<any> {
        return this.http.get("http://localhost:3000/search?search="+term);
    }
    searchCategory(categoryid): Observable<any> {
        return this.http.get("http://localhost:3000/getcategory?categoryid="+categoryid);
    }
     // get categories from database
     getAllCategories(): Observable<any> {
        return this.http.get("http://localhost:3000/categories/");
    }
    // get all orders from database
    getAllOrders(): Observable<any> {
        return this.http.get("http://localhost:3000/getorders/");
    }

    // add product to database
    addProduct(newProduct): Observable<any> {

        return this.http.post("http://localhost:3000/addproduct/", newProduct, this.httpOptions);
    }
    editProduct(productToEdit) {
        return this.http.post("http://localhost:3000/updateprod/", productToEdit, this.httpOptions);
    }
  
    // Check user details for Log In
    checkLogin(userDetails) {
        return this.http.post("http://localhost:3000/login" ,userDetails, this.httpOptions);
    }



}
