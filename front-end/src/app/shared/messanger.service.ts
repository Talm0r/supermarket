import { Injectable } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessangerService {
  private subject: Subject<any> = new Subject<any>();
  constructor() { }
// Emit message
  public emit(msg: any): void {
    this.subject.next(msg);
  }
// Clear 
  public clear(): void {
    this.subject.next();
  }

  // Get Observable
  public getObservable(): Observable<any> {
    return this.subject.asObservable();
  }

  // Subscribe to get message from messanger
  public subscribe(fn: (value: any) => void): Subscription {
    return this.subject.asObservable()
      .subscribe(fn);
  }
}
