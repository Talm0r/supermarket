import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http: HttpClient) { }
// Get User Cart
  getUserCart(userId): Observable<any> {
   return this.http.get("http://localhost:3000/checkorder?customerid="+userId).pipe(tap((data)=>{
    
   }))
}

// Create Receipt 
getReceipt(id):Observable<any> {
  return this.http.get("http://localhost:3000/receipt?id="+id)
}
// Search in cart 
searchCart(productName, cartId): Observable<any> {
  return this.http.post("http://localhost:3000/searchCart",productName,cartId);
}
// Create new cart
createNewCart(customerid, date): Observable<any> {

  return this.http.get("http://localhost:3000/createcart?customerid="+customerid+"&date="+date)  
}

// Delete item from cart 
deleteItemFromCart(itemToDelete): Observable<any> {
  return this.http.post("http://localhost:3000/deletefromcart",itemToDelete);
}
// Delete all items from cart 

deleteAllItems(itemToDelete): Observable<any> {
  return this.http.get("http://localhost:3000/deleteallcart?id="+itemToDelete);
}
// Add item to cart 
addItemToCart(itemToAdd):Observable<any> {
  return this.http.post("http://localhost:3000/addtocart",itemToAdd)  
}
// Create new cart 


// Make order 
makeOrder(orderDetails):Observable<any> {
  return this.http.post("http://localhost:3000/setorder",orderDetails)
}
// Check last user order 
checkLastOrder(customerid):Observable<any> {
return this.http.get("http://localhost:3000/checkorder?customerid="+customerid);
}
}
