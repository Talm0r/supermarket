import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { User } from '../models/user';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class UserService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    contactMessagesEmitter: EventEmitter<string> = new EventEmitter();
    contactEditEventEmiter: EventEmitter<User> = new EventEmitter();



    constructor(private http: HttpClient) {
    }

    isLoggedIn(): boolean {
        // check session , if true return true if false return false
        if (sessionStorage.getItem('loggedIn') != null) {
            return true;
        }
        else {
            return false;
        }
    }
    // Get user details 

    getDetails(userId): Observable<any> {
        return this.http.get("http://localhost:3000/getuserdetails?id=" + userId, this.httpOptions)
    }

    // add user to database
    addUser(newUser): Observable<any> {

        return this.http.post("http://localhost:3000/register/", newUser, this.httpOptions);
    }

    // delete  user from database
    delete(objId: string): Observable<any> {
        return this.http.post("http://localhost:3000/delete?_id=" + objId, this.httpOptions);
    }

    // Check user details for Log In
    checkLogin(userDetails) : Observable<any> {

        return this.http.post("http://localhost:3000/login", userDetails, this.httpOptions);

        // create session 
    }
    checkIfIdExists(id) : Observable<any> {
        return this.http.get("http://localhost:3000/checkid?id="+id);
    }
    checkIfEmailExists(email): Observable<any> {
        return this.http.get("http://localhost:3000/checkemail?email="+ email);
    }
}
